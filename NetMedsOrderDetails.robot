*** Settings ***
Documentation    Suite description
Library   Selenium2Library
Library   netmeds.py

Suite Setup      monitor build start time
Suite Teardown   monitor build end time
*** Variables ***

${user_name} =   medicineorders@docsapp.in
${password} =  docsapp123
${netmeds_login_url} =  https://www.netmeds.com/customer/account/login/
${staus_xpath} =  xpath=//*[@class="shipmenttrackmain"]
${login_button} =  //*[@id="send2"]

*** Keywords ***

monitor build start time
     ${build_start_time} =	Get Time

monitor build end time

     ${build_end_time} =	Get Time

input orderID to fetch details
    ${order_id}   get_orderId
    Set Suite Variable    ${order_id}

complete login in netmeds
        open browser   ${netmeds_login_url}  headlesschrome
        wait until keyword succeeds  5x  2s  input text   //*[@id="email"]   ${user_name}
        input text   //*[@id="pass"]    ${password}
        click element  ${login_button}

get details
    ${status} =  get text  ${staus_xpath}
    Set Suite Variable     ${status}
#    Set Test Variable      ${status}
    log to console         ${status}

check status of entered orderID
         go to  https://www.netmeds.com/orderhistory/customer/view/id/${order_id}
         ${visible} =  Run Keyword And Return Status   wait until element is visible   //*[@class="shipmenttrackmain"]  10s
         run keyword if   ${visible}  get details   ELSE  log to console  orderID not found after soo many attempts

*** Test Cases ***
Enter order id of to fetch order details ------->>>>>
    input orderID to fetch details
    complete login in netmeds
    check status of entered orderID






